ACCOUNT_PATH: str = "account.json"
FUNCTIOIN_PATH: str = "function.py"
DATASTRUCTURE_PATH: str = "datastructure.py"

import os
import sys
import json
import hashlib
from hashlib import sha256
import re   
import Crypto
from Crypto.PublicKey import RSA
import time
import secrets
import pbkdf2

class Account:
    def __init__(self, Email: str, FullName: str, Dob: str, PhoneNumber: str, Address: str, password: str, publicKey:str = None, privateKey: str= None) -> None:
        self.email = Email
        self.name = FullName
        self.dob = Dob
        self.phonenumber= PhoneNumber
        self.address = Address
        self.password = password
        if  publicKey == None:
            self.publicKey:str = ""
        else:
            self.publicKey:str = publicKey
       
        if  privateKey == None:
            self.privateKey:str = ""
        else:
            self.privateKey:str = publicKey       
    
    def change_password(self, new_password: str):
        self.password = new_password
    
    def print_account(self):
        print(self.email, self.name, self.dob, self.phonenumber,self.address, self.password, self.publicKey, self.privateKey, sep='\n')
        
        
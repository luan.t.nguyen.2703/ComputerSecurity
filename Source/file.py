from datastructure import *

def convert_account_to_json_format(acc: Account) -> dict:
    return  {
            'email' : acc.email,
            'name' : acc.name, 
            'dob' : acc.dob,
            'phonenumber' : acc.phonenumber,
            'address' : acc.address,
            'password' : acc.password
            }
  
def is_empty_file(file_path: str) ->bool:
    return os.stat(file_path).st_size == 0

def read_account_list(file_path: str) ->dict:
    
    with open(file_path, mode= 'r') as json_file: 
        accountList = json.load(json_file)
        
    json_file.close()  
    
    return accountList  
  
def write_file(file_path: str, data: dict) -> None:
    json_object = json.dumps(data, indent= 4)
    
    with open(file_path, "w") as outfile:
        outfile.write(json_object)
    
    outfile.close()
       
def check_validation_account(account: dict, file_path: str, mode: int = 0) -> bool:
    """
    mode = 0: check 
    mode = 1: add account to db
    
    """
    
    #email: str = account['email']
    
    # with open(file_path, mode= 'r') as json_file: 
    #     accountList = json.load(json_file)
    # json_file.close()
    
    accountList = read_account_list(file_path)
    
    for each_account in accountList['Account']:
            if (account['email'] == each_account['email']):
                return False     
                
    if (mode == 1) :
        accountList['Account'].append(account)
        #print(accountList)
        write_file(file_path, accountList)
    return True





def add_account_to_db(acc: dict, file_path: str) -> bool:
    
    # check empty file
    if is_empty_file(file_path) == True:
        
        List = {}
        List['Account'] = []
        List['Account'].append(acc)
        
        write_file(file_path,List)
        
        return True
    
    #not empty
    else:
        return check_validation_account(acc, file_path, 1)
            
            
    

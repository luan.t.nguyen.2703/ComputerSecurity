from datastructure import *
from file import add_account_to_db, read_account_list, write_file 
  
  
def is_not_valid_email(email) -> bool:  
     
    #regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    
    
    regex = '^[a-z0-9]+[@]\w+[.]\w{2,3}$'
    if re.search(regex,email):
        return False  
    return True


def convert_account_to_json_format(acc: Account) -> dict:
    return  {
            'email' : acc.email,
            'name' : acc.name, 
            'dob' : acc.dob,
            'phonenumber' : acc.phonenumber,
            'address' : acc.address,
            'password' : acc.password,
            'publicKey': acc.publicKey,
            'privateKey': acc.privateKey
            }
    

def enterAccountInformation():
    #"""họ tên, ngày sinh, điện thoại, địa chỉ, mật khẩu (password)"""
    
    Email: str = input("Please enter your email: ")
    while(is_not_valid_email(Email)):
        Email: str = input("Please enter correct email: ")
        
    FullName: str = input("Please enter your full name: ")
    
    Dob: str = input("Please enter your date of birth: ")
    
    PhoneNumber: str = input("Please enter your phone number: ")
    
    Address: str = input("Please enter your address: ")
    
    password: str = input("Please enter your password: ")
    while(len(password) < 8):
        password: str = input("Password length must be more or equal 8: ")
    
    return convert_account_to_json_format(Account(Email= Email, FullName= FullName, Dob= Dob, PhoneNumber= PhoneNumber, Address= Address, password= password))

def hash_salt_password(email: str, password: str) -> str:
    
    salt = password[3]
    hashed_password = hashlib.sha256((salt + password).encode()).hexdigest()
    
    return hashed_password

def register() -> bool:
    
    account: dict = enterAccountInformation()
    
    new_pass = {'password': hash_salt_password(account['email'], account['password'])}
    
    account.update(new_pass)
    
    return add_account_to_db(account, ACCOUNT_PATH)

def login(email: str, password: str) -> bool:
    
    with open(ACCOUNT_PATH, 'r') as json_file:
        data = json.load(json_file)
    json_file.close()
       
    for acc in data['Account']:
        if email == acc['email']:
            if hash_salt_password(email, password) == acc['password']:
                #print("Login successfully\n")
                return True
            
    #print("Incorrect email or password")
    return False    


def encrypt_private_key(password: str):
    
    secret_key = pbkdf2.PBKDF2()
    iv = secrets.randbits(256)

    

def generate_key_pair(email: str)-> dict:
    
    new_key = RSA.generate(2048)

    private_key = new_key.exportKey()    
    public_key = new_key.publickey().exportKey()
    


    start_pri: int = len("-----BEGIN RSA PRIVATE KEY-----\n")
    end_pri: int = len("\n-----END RSA PRIVATE KEY-----")
    
    start_pub: int = len("-----BEGIN PUBLIC KEY-----\n")
    end_pub: int = len("\n-----END PUBLIC KEY-----")
    
    private_key_rm = private_key[start_pri: len(private_key)- end_pri]
    public_key_rm = public_key[start_pub: len(public_key) - end_pub]
        
    accountList: dict = read_account_list(ACCOUNT_PATH)
    
    for each in accountList['Account']:
        if(each['email'] == email):
            if(each['privateKey'] == ""):
                each.update({'privateKey': private_key_rm.decode("utf-8"), 'publicKey': public_key_rm.decode("utf-8")})
                break
            else: 
                select: str = input("The key-pair has been generated before. Do you want to generate new key-pair ?  (Y/n) \n")
                
                if(select == "Y"):
                    each.update({'privateKey': private_key_rm.decode("utf-8"), 'publicKey': public_key_rm.decode("utf-8")})
                    break
                else:
                    return
    write_file(ACCOUNT_PATH, accountList)
    
    
    print(private_key.decode("utf-8"), public_key.decode("utf-8"), sep='\n')
    
    print("\nGenerate key-pair successfully!\nThe key-pair has been added to your account\n")
    
    return {'privateKey': private_key.decode("utf-8"), 'publicKey': public_key.decode("utf-8")}




def update_information(email: str, password:str = None) -> None:
    
    accountList = read_account_list(ACCOUNT_PATH)
    
    for each_account in accountList['Account']:
        if (email == each_account['email']):
            
            select_0: str = "1"
            while (int(select_0) >=1 and int(select_0) <= 5):
                os.system("cls")
                print("Current Information\n", "email: " + each_account['email'],"1. name: " + each_account['name'], "2. dob  : " + each_account['dob'], "3. Phone number: " + each_account['phonenumber'], "4. Address: " + each_account['address'], "5. Password: ", sep='\n')
                
                select_0 = input("\n1. Full name\n2. Date of birth\n3. Phone nummber\n4. Address\n5. Password\n6. Save and Exit\n0.Exit\nSelect: ")
                new_infor = ""
                if(select_0 == '1'):
                    new_infor = input("New name: ")
                    each_account.update({'name': new_infor})
                elif select_0 == '2':
                    new_infor = input("New dob: ")
                    each_account.update({'dob': new_infor})
                elif select_0 == '3': 
                    new_infor = input("New phone number: ")
                    each_account.update({'phonenumber': new_infor})
                elif select_0 == '4': 
                    new_infor = input("New address: ")
                    each_account.update({'address': new_infor})
                elif select_0 == '5': 
                    old_pass = input("Current password: ")
                    
                    if len(old_pass) < 8:
                        print("Incorrect password!")
                        time.sleep(2)
                    else: 
                        if hash_salt_password(email, old_pass) == each_account['password']:
                            new_infor = input("New password: ")
                            each_account.update({'password': hash_salt_password(email, new_infor)})
                        else:
                            print("Incorrect password!")
                            time.sleep(2) 
                elif select_0 == '6':
                    write_file(ACCOUNT_PATH, accountList)
                    print("\nDone\n")
                else: 
                    break    
    os.system("Pause")
    
           
                
                        
from datastructure import *
from functions import *
from file import *

def menu():
    
    choose_0 = '1'
    
    while(choose_0 == '1' or choose_0 == '2'):
        #time.sleep(2)
        os.system('cls')
        print("1. Sign in\n2. Sign up\n0. Exit")
        choose_0 = input("Select: ")
        
        if (choose_0 != '1'  and choose_0 != '2'):
            return
        elif choose_0 == '2':
            regis = register()
            if(regis):
                print("Register successfully!\n")
            else:
                print("This email has been used before, please use other email\n")
            os.system("pause")
        elif choose_0 == '1':
            
            email: str = input("Email: ")
            if is_not_valid_email(email):
                print("Not valid email")
                time.sleep(2)
            else:
                password = input("Password: ")
                
                check_1 = login(email, password)
                
                if(check_1 == 0):
                    print("\nIncorrect email or password\n")
                    time.sleep(2)
                else:
                    print("\nLogin successfully\n\n")
                    os.system("pause")
                    os.system('cls')
                    
                    choose_1 = '1'
                    
                    while(choose_1 == '1' or choose_1 == '2'):
                        os.system('cls')
                        print("1. Generate asymmetric key pair\n2. Edit information\n0. Exit\n")
                        choose_1 = input("Select: ")
                        if(choose_1 == '1'):
                            generate_key_pair(email)
                            os.system("pause")
                        elif choose_1 == '2':
                            update_information(email)
    
    
    return 

def main():
    menu()

if __name__ == "__main__":
    main()